import { handleResponse, handleError } from "./apiUtils";

const url = "http://air-pollution-api.herokuapp.com/";

export function getStationsDataG(bounds) {
  const data = {
    query: `{ getStations(minLat: ${bounds[0]}, minLong: ${
      bounds[1]
    }, maxLat: ${bounds[2]}, maxLong: ${
      bounds[3]
    }) { name, latitude, longitude, aqi } }`
  };

  return fetch(url, {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(handleResponse)
    .catch(handleError);
}

export function getCurrentPollutionDataG() {
  const data = {
    query:
      "{ getLatest(lat: 33.831, long: -117.939) { latitude, longitude, aod } }"
  };

  return fetch(url, {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(handleResponse)
    .catch(handleError);
}

export function getHistoricalDataG(lat, lng) {
  const data = {
    query: `{  getHistorical(lat: ${lat}, long:${lng}, n:30) { latitude, longitude, aod } }`
  };

  return fetch(url, {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(handleResponse)
    .catch(handleError);
}
