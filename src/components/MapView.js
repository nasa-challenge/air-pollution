import React, { Component } from "react";
import { getCurrentPollutionDataG } from "../api/mapsApi";
import { geolocated } from "react-geolocated";
import MapWithMarkers from "./MapWithMarkers";

class MapView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  async componentDidMount() {
    let nativeGeolocation = navigator.geolocation;
    nativeGeolocation.getCurrentPosition(() => {});

    const data = await getCurrentPollutionDataG();
    this.setState({
      data: data.data.getLatest
    });
  }

  render() {
    const defaultCenter = {
      lat: 34.052235,
      lng: -118.243683
    };
    const coords = this.props.coords;
    const center = {
      lat: coords && coords.latitude,
      lng: coords && coords.longitude
    };
    return (
      <div style={{ height: "100vh", width: "100%" }}>
        <MapWithMarkers
          markers={this.state.data}
          center={coords && center}
          defaultCenter={defaultCenter}
        />
      </div>
    );
  }
}

export default geolocated()(MapView);
