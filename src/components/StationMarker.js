import React from "react";
import "./maps.css";

function StationMarker(props) {
  function onMarkerClick() {
    // props.onClick(props.lng, props.lat);
  }
  const sizeString = props.sizeString;
  return (
    <button
      onClick={onMarkerClick}
      className="stationMarker"
      style={{ width: sizeString, height: sizeString }}
    >
      {props.text}
    </button>
  );
}

export default StationMarker;
