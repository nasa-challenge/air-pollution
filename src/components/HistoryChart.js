import React, { Component } from "react";
import { Chart } from "react-google-charts";
import "./maps.css";

import { getHistoricalDataG } from "../api/mapsApi";
class HistoryChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  async componentDidMount() {
    const data = await getHistoricalDataG(this.props.lat, this.props.lng);
    this.parseArray(data.data.getHistorical);
  }
  componentDidUnmount() {
    this.setState({
      data: []
    });
  }
  parseArray(arr) {
    let parsedData = [];
    for (let i in arr) {
      const c = arr[i];
      if (Number(c.aod) !== -1) {
        parsedData.push([Number(i), Number(c.aod)]);
      }
    }
    this.setState({
      data: parsedData
    });
  }

  onClose = () => {
    this.props.onClose();
  };

  render() {
    return (
      <div className="chartBox">
        <div
          style={{
            justifyContent: "flex-end",
            display: "flex"
          }}
        >
          <button onClick={this.onClose}>X</button>
        </div>
        <div>
          {this.state.data.length !== 0 ? (
            <Chart
              chartType="LineChart"
              data={[["Day", "aod1"], ...this.state.data]}
              loader={<div>Loading Chart</div>}
              width="400px"
              height="200px"
              legendToggle
            />
          ) : null}
        </div>
      </div>
    );
  }
}

export default HistoryChart;
