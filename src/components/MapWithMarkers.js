import React, { Component } from "react";
import GoogleMapReact from "google-map-react";
import HistoryChart from "./HistoryChart";
import Marker from "./Marker";
import StationMarker from "./StationMarker";
import "./maps.css";
import { getStationsDataG } from "../api/mapsApi";

class MapWithMarkers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 0,
      lng: 0,
      chart: false,
      stations: [],
      sizeString: "25px"
    };
  }

  onMarkerClick = (lng, lat) => {
    this.setState({
      lat: lat,
      lng: lng,
      chart: true
    });
  };

  onHistoryChartClose = () => {
    this.setState({
      chart: false
    });
  };

  async getStations(bounds) {
    let nativeGeolocation = navigator.geolocation;
    nativeGeolocation.getCurrentPosition(() => {});

    const stations = await getStationsDataG(bounds);
    this.setState({
      stations: stations.data.getStations
    });
  }

  render() {
    const markers = this.props.markers;
    const center = this.props.center
      ? this.props.center
      : this.props.defaultCenter;

    return (
      <div style={{ height: "100vh", width: "100%" }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API_KEY }}
          center={center}
          defaultZoom={8}
          onBoundsChange={(center, zoom, bounds, marginBounds) => {
            this.getStations(bounds);

            const k = zoom / 8;
            const size = k * 25;
            const sizeString = size + "px";
            this.setState({ sizeString: sizeString });
          }}
        >
          {markers &&
            markers.map(m => (
              <Marker
                key={m.longitude + m.latitude}
                lat={Number(m.latitude)}
                lng={Number(m.longitude)}
                text={m.aod}
                onClick={this.onMarkerClick}
              />
            ))}
          {this.state.stations &&
            this.state.stations.map(m => (
              <StationMarker
                sizeString={this.state.sizeString}
                key={m.name}
                lat={Number(m.latitude)}
                lng={Number(m.longitude)}
                text={m.aqi}
                onClick={this.onMarkerClick}
              />
            ))}
          {this.state.chart ? (
            <HistoryChart
              lat={this.state.lat}
              lng={this.state.lng}
              onClose={this.onHistoryChartClose}
            />
          ) : null}
        </GoogleMapReact>
      </div>
    );
  }
}

export default MapWithMarkers;
