import React from "react";
import "./maps.css";

function Marker(props) {
  function onMarkerClick() {
    props.onClick(props.lng, props.lat);
  }

  return (
    <button onClick={onMarkerClick} className="marker">
      {props.text}
    </button>
  );
}

export default Marker;
