import React, { Component } from "react";
import GoogleMapReact from "google-map-react";

class MapExample extends Component {
  handleApiLoaded(map, maps) {
    maps.event.addListener(map, "idle", function() {
      const centerLat = map.getCenter().lat();
      const centerLng = map.getCenter().lng();
      var bounds = map.getBounds();
      const lng1 = bounds.ka.g;
      const lng2 = bounds.ka.h;
      const lat1 = bounds.oa.h;
      const lat2 = bounds.oa.h;
      // console.log(lng1 + "___" + lng2 + "___" + lat1 + "___" + lat2 + "___");
    });
  }

  render() {
    const center = this.props.center;
    return (
      <div style={{ height: "100vh", width: "100%" }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API_KEY }}
          center={center ? center : this.props.defaultCenter}
          defaultZoom={8}
          heatmapLibrary={true}
          heatmap={this.props.data}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={({ map, maps }) => this.handleApiLoaded(map, maps)}
        ></GoogleMapReact>
      </div>
    );
  }
}

export default MapExample;
