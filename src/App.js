import React from "react";
import "./App.css";

import { MapView } from "./components";

function App() {
  return (
    <div className="App">
      <MapView />
    </div>
  );
}

export default App;
